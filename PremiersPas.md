# Premiers pas

Les opérations de configuration du PC et du Raspberry Pi sont assez longues et devraient être faites en temps masqué.
Il est donc recommandé de les lancer, et de faire le montage du robot pendant ce temps.

La configuration pour le Raspberry Pi et le PC ne sont valables que dans le cas où l'on souhaite utiliser le robot avec le Raspberry Pi fourni et ROS Kinetik Kame.
Après les tests de base avec le Raspberry, nous l'avons remplacé par une carte NIDIA Jetson Nano, afin d'effectuer du calcul graphique sur le robot.
Dans ce cas, ne faire que la configuration de la carte OpenCR, et aller voir [le dépôt dédié](https://gitlab.utc.fr/hds/turtlebot/installation-nvidia-jetson-nano) pour l'installation de ROS sur le PC et la carte NVIDIA.

## Montage du robot

La notie de montage du robot est assez complète. Une seule information manque réellement : quel est l'avant du robot.
De manière assez peu intuitive, l'avant du robot est la partie la plus large de celui-ci. La roue folle est donc située à l'arrière.

Il est capital de monter le robot dans le bon sens pour son bon fonctionnement.

## Configuration du PC

Cette section est dédiée à la configuration de l'ordinateur de l'utilisateur (ordinateur distant et non pas le Raspberry embarqué)

ROS est disponible uniquement sur Ubuntu et Debian.
Pour l'une de ces distributions, il existe une [documentation d'installation](http://emanual.robotis.com/docs/en/platform/turtlebot3/pc_setup/) bien renseignés.
* Les paquets ROS à installer ne sont disponibles que pour **Ubuntu 16.04 LTS** et non pas pour la version 18.04
* Il faut penser à bien redémarrer la machine après l'installation de ROS ou à sourcer les bons fichiers (`/opt/ros/kinetic/setup.bash`), sans quoi la commande `catkin_make` ne fonctionnera pas.

Pour les autres distributions, il n'existe pas de paquets officiels (présence de paquets sur l'AUR).
On préfèrera installer une machine virtuelle Ubuntu. On veillera alors à utiliser un pont plutôt qu'un NAT pour le réseau, afin de placer directement la VM sur le même réseau que le robot.

Il existe également des [images Docker](https://hub.docker.com/_/ros?tab=description) basées sur Ubuntu et avec les paquets ROS d'installés.
Elles n'ont pas été essayées notamment car il est alors plus complexe de mettre en place le réseau, et une interface graphique.

## Configuration du Raspberry Pi

**Attention** cette opération requiert de l'énergie et du temps.
Il est donc recommandé de ne pas utiliser la batterie mais de brancher directement le robot.

Deux installations sont possibles pour le Raspberry Pi :
* Installation de ROS **sur ubuntu MATE**
* Installation d'une **Raspbian dérivée** fournie pas Robotis et proposant tous les paquets de base nécessaire.

On préfèrera utiliser la seconde solution.
En effet, Raspbian est **plus léger** et conçu pour fonctionner directement sur le Raspberry Pi.
Par ailleurs, cela nous donnera une distribution clef en main pour jouer avec notre robot.

La documentation pour installer le Raspberry Pi est disponible [ici](http://emanual.robotis.com/docs/en/platform/turtlebot3/raspberry_pi_3_setup/#install-linux-based-on-raspbian)

À noter :
* Le SHA256 donné sur le site est celui de l'image dézippée et non pas de l'archive. On le vérifie donc après avoir récupéré l'image.
* L'extraction de l'image du dossier zip se fait avec la commande `unzip`. Celle-ci est relativement longue.
* Pour écrire l'image dans la carte du Raspberry est faite avec la commande `dd bs=4M status=progress if=image_rpi_XVERSION.img of=/dev/mmcblk0` *verifier le nom de l'image et le périphérique de sortie*.

## Configuration de la carte OpenCR

Pour la carte, on va mettre à jour le firmware.

Les instructions pour la mise en place du firmware sont disponibles sur [cette page](http://emanual.robotis.com/docs/en/platform/turtlebot3/opencr_setup/#opencr-setup)

On utilisera la méthode recommandée (par script shell), en faisant attention de bien exécuter les instructions pour le modèle Burger.
Elles ont été exécutées directement sur le Raspberry Pi du robot, sur lequel est branchée la carte.

## Robot Model

Certains nœuds ont besoin de connaître le modèle turtlebot que l'on utilise pour fonctionner.
Ils utilisent en général la variable d'environement `TURTLEBOT3_MODEL`.

On ajoutera donc dans les `bashrc` de la carte et du PC :
```bash
export TURTLEBOT3_MODEL=burger
```

## Lancer Rviz

L'environnement Rviz nous permet de visualiser de nombreuses infirmations sur le robot et son environnement (dont les résultats du scann du LIDAR).

Les actions à effectuer pour lancer cette applications sont décrites [ici](http://emanual.robotis.com/docs/en/platform/turtlebot3/bringup)

Le résultat obtenu est le suivant :

![Rviz](./rviz.png)

On y visualise le robot dans un environnement 3D. Les lignes rouges situés à distance du robot sont les résultats du LIDAR.
