# Basics

*Ces instructions sont à lancer après avoir suivi la section [Premiers Pas](./PremiersPas.md) et avec roscore et le cœur du robot lancés*

## Catkin build

Lorsque l'on utilise la commande `catkin_make` pour la construction des paquets, on peut se retrouver avec des problèmes si certains paquets doivent être construits de manière isolée.
Il est alors recommandé de remplacer la commande `catkin_make` par la commande `catkin build`, qui prend correctement en charge ce genre de cas.
Elle est fournie par le paquet `python-catkin-tools`. Elle doit être exécutée une fois dans le dossier `~/catkin_ws` puis pourra ensuite être lancée depuis n'importe quel sous dossier du workspace.

## Monitoring du robot

Maintenant qu'on a lancé le robot, on va monitorer les différents « topics » qui le composent.

Pour cela, on utilise le framework de développement [rqt](http://wiki.ros.org/rqt).

Cet outil nous permet de visualiser les différents messages envoyés sur les topics du robot.

Plus d'explications sont disponibles [ici](http://emanual.robotis.com/docs/en/platform/turtlebot3/topic_monitor/)

## Télécommande pour le robot

Les instructions pour le contrôle à distance du robot sont données sur [cette page](http://emanual.robotis.com/docs/en/platform/turtlebot3/teleoperation/)

### Contrôle par le clavier du Remote PC

Pour cela, il suffit de lancer sur le Remote PC les instructions suivantes :

```bash
$ export TURTLEBOT3_MODEL=burger
$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```

On peut ensuite contrôler le robot avec les touches du clavier (mappé pour un clavier qwerty):
* w/x pour augmenter/diminuer la vitesse linéaire
* a/d pour augmenter/réduire la vitesse angulaire
* espace ou s pour tout stopper

### Contrôleur PS3

Après avoir appairé le contrôleur PS3 au Remote PC via bluetooth, on lance les paquets de téléopération.
L'erreur suivante est alors lancée :

```
[ERROR] [1562343617.268329523]: Couldn't open joystick force feedback!
```
Pour le moment, je n'ai pas réussi à piloter le robot avec ce contrôleur

### Contrôleur XBox360

En branchant un contrôleur de XBox360 sur le robot et en suivant les instructions fournies dans la documentation (+ lancer un `sudo rmmod xpad`), j'ai pu piloter le robot.
À noter, il faut appuyer sur le joystick de droite, et diriger avec celui de gauche.

Curieusement, le même message d'erreur que pour le contrôleur PS3 s'affiche, mais cela ne pose visiblement pas de soucis majeur.

En essayant une deuxième fois, je n'ai pas réussi à faire refonctionner le contrôleur XBox

### Contrôle par Android

En suivant les instructions données dans la documentation, j'ai aussi pu contrôler le robot par Android.

## Autres exemple de contrôles

[Cette page](http://emanual.robotis.com/docs/en/platform/turtlebot3/basic_examples/#basic-examples) montre d'autres exemple de contrôle du robot.

### Marqueurs interactifs

Cette fonction permet de contrôler le robot depuis Rviz, en déplaçant des marqueurs sur la carte interactive.

### Détection d'obstacle

Cette fonction devrait permettre au robot de s'arrêter de lui-même quand il détecté un obstacle.
Pas réussi à la faire fonctionner pour le moment //TODO

### Déplacement par points dans l'espace

Permet de déplacer le robot sur un point absolu de l'espace (position x, position y, angle). OK

### Patrouille

Donner une forme de patrouille (triangle, rectangle ou cercle) avec ses paramètres et un nombre de fois où effectuer la patrouille.

## SLAM

### Tracer une carte

[Page du SLAM](http://emanual.robotis.com/docs/en/platform/turtlebot3/slam/#run-slam-nodes)

Le SLAM a d'abord été essayé dans la configuration Raspberry Pi du robot (ROS Kinetic Kame). Il fonctionne correctement.
Il est conseillé d'utiliser l'algorithme **cartographer**.
En effet, la plupart des autres algorithmes sont basés sur **gmapping**, qui est déprécié.

La version de cartographer présente dans les paquets pour ROS Kinetic Kame est la version 0.2.
Pour installer une version plus récente, on peut le faire directement depuis les sources.

On pourra alors se servir de `catkin build`, ce paquet devant être construit de manière isolée.

On installera donc le paquet de la manière suivante :

```console
$ cd ~/catkin_ws/src
$ git clone https://github.com/googlecartographer/cartographer.git
$ git clone https://github.com/googlecartographer/cartographer_ros.git
$ catkin build
```

Le SLAM (Simultaneous Localization And Mapping) est une technique de construction de carte d'un environnement.
On utilise les capteurs du robot pour déterminer sa position, et son LIDAR pour trouver les obstacles par rapport à la position du robot.
On est alors capable de dessiner une carte des différents obstacles dans le plan. 

Cartographer utilise ainsi l'odométrie du robot et sa centrale inertielle pour estimer la position relative du robot par rapport à son point de départ.
Pour les cartes en 2 dimensions (seules possibles avec notre LIDAR une couche), on pourra changer la configuration de cartographer afin de désactiver l'IMU ou l'odométrie.

L'algorithme de cartographie est également capable de corriger les dérives de l'odométrie et de l'IMU en se servant du LIDAR.
Quand le robot repasse par un endroit où il est déjà allé, l'algorithme corrige la carte et la pose du robot afin que les obstacles détectés coïncident avec ceux déjà tracés sur la carte. 

Pour l'exploration de l'environnement, 

La méthode cartographer n'a été testée que sous ROS Melodic avec la Jetson Nano. Pour plus de détails, voir le [dépôt dédié à l'installation avec la Jetson Nano](https://gitlab.utc.fr/hds/turtlebot/installation-nvidia-jetson-nano).

### Enregistrer la carte

Une fois la carte tracée avec le SLAM, on peut l'enregistrer pour s'en servir par la suite. Pour cela, on utilisera le nœud `map_saver` :

```console
$ rosrun map_server map_saver -f ~/map
```

Cette commande sauvegardera la carte en cours dans les fichiers `~/map.pgm` et `~/map.yaml`.
On peut évidement changer l'option `-f` de la commande précédente pour enregistrer la carte sous un autre nom.

## Navigation

Voir [la page sur la navigation](http://emanual.robotis.com/docs/en/platform/turtlebot3/navigation/#run-navigation-nodes)

Un fois la carte tracée, on peut s'en servir pour faire naviguer le robot à l'intérieur de celle-ci.
On pourra alors donner au robot un objectif sur la carte, et il calculera alors un chemin pour y arriver.

Pour cela, on lance les nœuds de navigation *(en ajustant le chemin de la carte enregistrée au préalable)* :
```console
$ roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/map.yaml
```

Il faut ensuite estimer la pose initiale du robot (localisation et orientation) grâce au bouton `2D Pose Estimate`.
Cela permet à ROS de connaître approximativement l'endroit où se situe le robot.
On va ensuite se servir du LIDAR pour repositionne correctement le robot.
ROS va en effet essayer de faire coller les données émises par le LIDAR avec les obstacles connus sur la carte.

Pour cela, on va utiliser la téléopération : on fait avancer et reculer un petit peu le robot jusqu'à ce que ROS ait correctement corrigé la position de celui-ci.

À noter que pour que cela fonctionne correctement, il est plus simple de positionner le robot non pas dans un couloir droit mais dans un angle, afin que le ROS puisse adapter la pose en deux dimensions du robot.

Une fois que le robot est correctement positionné sur la carte, on peut utiliser le bouton `2D Nav Goal` pour donner un objectif (position et orientation) au robot.
Celui-ci s'y rendra en prenant un chemin rendu possible par la carte.
Si un obstacle qui n'était pas présent sur la carte se trouve sur le chemin, le robot adaptera sa trajectoire pour l'éviter.
