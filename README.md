# TurtleBot3 - Pour commencer

## Introduction

L'objectif de ce dépôt est de fournir un maximum d'informations de base pour prendre en main le robot TurtleBot3 (modèle Burger).

Les sources utilisées pour ce projet sont le [manuel en ligne](http://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview)

Lors de la rédaction de cette documentation, le robot a d'abord été utilié avec le Raspberry Pi, tel que fourni, puis celui-ci a été remplacé par une carte NVIDIA Jetson Nano, ce qui nous a poussé à hanger la version de ROS installée (voir [ici](./PremiersPas.md) pour plus de détails).

## Fonctionnalités

Cette section vise à recenser de manière qualitative les principales fonctionnalités offertes par le TurtleBot.

### Standard ROS

Le robot TurtleBot est entièrement OpenSource et piloté par [OpenRobotics](https://www.openrobotics.org).
Il est donc piloté par [ROS](http://www.ros.org/) et est compatible avec d'autres composants pour robots basés sur ROS.

### Servomoteurs DYNAMIXEL

Le TurtleBot Burger est propulsé par deux servomoteurs [Dynamixel XL430-W250T](http://emanual.robotis.com/docs/en/dxl/x/xl430-w250/) proposant 4 modes d'opération :
* Contrôle de vitesse
* Contrôle de position
* Contrôle étendu
* Contrôle PWM

La [documentation](http://emanual.robotis.com/docs/en/dxl/x/xl430-w250/) du servomoteur est très fournie et explique comment utiliser ses différentes fonctionnalités.

### Carte de contrôle

La carte de contrôle du TurtleBot est une carte [OpenCR1.0](http://emanual.robotis.com/docs/en/parts/controller/opencr10/).
Développée pour les systèmes embarqué ROS, elle est complètement Open Source.

Elle est capable non seulement de piloter les deux servomoteurs DYNAMIXEL, mais propose également d'autres interfaces do communication :
* De nombreux pins, proposant une compatibilité avec ceux de l'Arduino Uno
* 4 rangées de pins OLLO
* 18 GPIO
* 6 PWM
* 1 I2C
* 1 SPI
* Une interface USB2.0 OTG
* 3 ports TTL (pilotage des moteurs)
* 3 ports RS485
* 2 UART
* 1 JTAG

Elle propose par ailleurs 4 LED pour l'utilisateur, ainsi que deux boutons.

### Capteurs

Le TurtleBot dispose d'un LIDAR 360° (modèle [LDS-01](http://emanual.robotis.com/docs/en/platform/turtlebot3/appendix_lds_01/)), ainsi que d'une centrale inertielle à 9 axes.

### Ordinateur

Le TurtleBot embarque son propre ordinateur, sous la forme d'un Raspberry Pi3 modèle B+.

## Spécifications techniques

Les spécifications techniques du TurtleBot Burger sont disponibles sur [cette page](http://emanual.robotis.com/docs/en/platform/turtlebot3/specifications/#specifications)

## Premiers pas

Voir [cette page](./PremiersPas.md) pour faire ses premiers pas avec le TurtleBot Burger.
